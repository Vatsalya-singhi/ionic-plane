import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class BookService {

    constructor() { }

    public passengerCount: number = 0;

    public submit(answer, number: any) {
        try {
            if (typeof answer == "string") {
                var array = JSON.parse(answer);
            } else {
                var array = answer;
            }

            if (Array.isArray(array) == false) {
                console.log('Array.isArray(array)=>', Array.isArray(array));
                return false;
            }

            if (!Number.isInteger(Number(number))) {
                console.log('Number.isInteger=>', Number.isInteger(Number(number)));
                return false;
            }
        } catch (err) {
            return false;
        }

        this.passengerCount = Number(number);


        console.log('arr=>', Array.isArray(array));

        var maxRow = array.map((row) => { return Math.max.apply(Math, row); });

        var colSize = Math.max.apply(Math, maxRow);
        var rowSize = Math.max.apply(Math, maxRow);

        //Identify seats
        var seats = this.fillWithMAandW(array);

        //Replace chars with numbers
        var obj: any = {};

        obj = this.replaceWithNumber("A", 1, seats, colSize, rowSize);
        obj = this.replaceWithNumber("W", obj.counter, obj.seats, colSize, rowSize);
        obj = this.replaceWithNumber("M", obj.counter, obj.seats, colSize, rowSize);
        seats = obj.seats;

        console.log('obj.seats=>', obj.seats);
        return seats;
    }

    public fillWithMAandW(array) {
        var seats = [];

        for (var i = 0; i < array.length; i++) {
            seats.push(Array(array[i][1]).fill(0).map(() => Array(array[i][0]).fill("M")));
        }

        for (var i = 0; i < seats.length; i++) {
            for (var j = 0; j < seats[i].length; j++) {
                seats[i][j][0] = "A";
                seats[i][j][seats[i][j].length - 1] = "A";
            }
        }

        for (var i = 0; i < seats[0].length; i++) {
            seats[0][i][0] = "W";
        }

        for (var i = 0; i < seats[seats.length - 1].length; i++)
            seats[seats.length - 1][i][(seats[seats.length - 1][i].length) - 1] = "W";

        return seats;
    }

    public replaceWithNumber(val, counter, seats, colSize, rowSize) {
        for (var i = 0; i < colSize; i++) {
            for (var j = 0; j < rowSize; j++) {
                if (seats[j] == null || seats[j][i] == null)
                    continue;
                for (var k = 0; k < seats[j][i].length; k++) {
                    if (seats[j] != null && seats[j][i] != null && seats[j][i][k] === val) {
                        if (counter <= this.passengerCount) {
                            seats[j][i][k] = counter;
                            counter++;
                        } else {
                            return { seats: seats, counter: counter };
                        }
                    }
                }
            }

        }
        return { seats: seats, counter: counter };
    }

    public printValues(seats, colSize, rowSize) {
        var stringJ = ""
        for (var i = 0; i < colSize; i++) {
            for (var j = 0; j < rowSize; j++) {
                if (seats[j] == null || seats[j][i] == null) {
                    stringJ += "----- " + ",";
                    continue;
                }
                for (var k = 0; k < seats[j][i].length; k++) {
                    if (typeof (seats[j][i][k]) != "number") {
                        stringJ += "- ";
                    } else {
                        stringJ += (seats[j][i][k] + " ");
                    }
                }
                stringJ += ",";
            }
            stringJ += "\n"
        }
        console.log(stringJ)
        return stringJ;
    }

}
