import { TestBed } from '@angular/core/testing';

import { BookService } from './book.service';

describe('BookService', () => {
    let service: BookService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(BookService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });


    it('test1 - normal case 1', () => {
        let arr = [[3, 2], [4, 3], [2, 3], [3, 4]];
        let output = service.submit(arr, 30);

        expect(Array.isArray(output)).toBeTruthy();
    })

    it('test2 - normal case 2', () => {
        let arr = "[[3, 2], [4, 3], [2, 3], [3, 4]]";
        let output = service.submit(arr, 30);

        expect(Array.isArray(output)).toBeTruthy();
    })

    it('test3 - passengerCount as string', () => {
        let arr = "[[3, 2], [4, 3], [2, 3], [3, 4]]";
        let output = service.submit(arr, "25");

        expect(Array.isArray(output)).toBeTruthy();
    })


    it('test4 error case Not an Array ', () => {
        let arr = "{'a':1,'b':1,'c':1,'d':1,'e':1,'f':1, }";
        let output = service.submit(arr, 20);

        expect(output).not.toBeTruthy();
    })


    it('test5 error case Improper dictonary', () => {
        let arr = "{:1,'e':1,'f':1, }";
        let output = service.submit(arr, 20);
        console.log('output=>', output);

        expect(output).not.toBeTruthy();
    })

    it('test6 error case empty array', () => {
        let arr = "";
        let output = service.submit(arr, 20);
        console.log('output=>', output);

        expect(output).not.toBeTruthy();
    })

    it('test7 error PassengerCount not a number', () => {
        let arr = "[[3, 2], [4, 3], [2, 3], [3, 4]]";
        let output = service.submit(arr, "MURNRUNR");

        expect(output).not.toBeTruthy();
    })

});
