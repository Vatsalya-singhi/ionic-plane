import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookService } from '../common/book.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    public planForm: FormGroup;
    public message: any = null;

    public arrData: any = [];

    constructor(
        private formBuilder: FormBuilder,
        public bookService: BookService,
    ) {

    }

    public ngOnInit(): void {
        this.buildForm();
    }


    public buildForm(): void {
        this.planForm = this.formBuilder.group({
            arr: ['[ [3,2], [4,3], [2,3], [3,4] ]', Validators.required],
            passengerCount: [30, [Validators.required, Validators.min(1)]],
        });
    }

    public submit() {
        let data = this.planForm.value;
        console.log('data=>', data);
        try {
            let x = this.bookService.submit(this.arr.value, this.passengerCount.value);
            console.log('x=>', x);
            if (typeof x == "boolean") {
                return false;
            }
            if (Array.isArray(x)) {
                this.arrData = x;
                return true;
            }
        } catch (err) {
            console.log('err', err);
        }

        return false;
    }


    public CheckNAN(val) {
        return Number.isInteger(Number(val)) ? val : "--";
    }

    public getColor(i, x, arrBlock: any[]) {
        if ((i == 0 && x == 0) || (i == this.arrData.length - 1 && x == arrBlock.length - 1)) {
            return "success";
        }

        if ((x == 0) || (x == arrBlock.length - 1)) {
            return "primary";
        }

        return "danger";
    }
    /**************************************************************
     ****************Get Functions*****************************
     *************************************************************/

    get arr() {
        return this.planForm.get('arr');
    }

    get passengerCount() {
        return this.planForm.get('passengerCount');
    }
}
