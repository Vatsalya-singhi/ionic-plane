import { browser, by, element } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get('/');
    }

    getParagraphText() {
        return element(by.deepCss('app-root ion-content')).getText();
    }

    getForm() {
        return element(by.id('form'));
    }

    getArrayFormControl() {
        // return element(by.id('arrInput'));
        return element(by.css('ion-input[formControlName="arr"] input'));
    }

    getPassengerCountFormControl() {
        // return element(by.id('passengerCount'));
        return element(by.css('ion-input[formControlName="passengerCount"] input'));
    }

    getsubmitBtn() {
        return element(by.id('submitBtn'));
    }


    getFoo() {
        return element.all(by.className('foo'));
    }

}
