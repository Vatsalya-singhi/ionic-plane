import { AppPage } from './app.po';
import { browser } from 'protractor';

describe('new App', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    // it('loading form', () => {
    //     page.navigateTo();
    //     browser.sleep(3000);

    //     expect(page.getForm().isPresent()).toBeTruthy();
    // });

    it('Fill form', async () => {
        page.navigateTo();
        browser.sleep(5000);

        page.getArrayFormControl().clear();
        page.getArrayFormControl().sendKeys("[ [3,2], [4,3], [2,3], [3,4] ]");

        page.getPassengerCountFormControl().clear();
        page.getPassengerCountFormControl().sendKeys(30);

        
        page.getsubmitBtn().click();

        browser.sleep(3000);

        let length = await page.getFoo().count();

        expect(length > 0).toBeTruthy();
    });

});
